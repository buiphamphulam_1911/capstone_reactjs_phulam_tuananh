import "./App.css";
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";
import "antd/dist/antd.css";
import HomePage from "./Components/HomeMovie/HomePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import DetailMovie from "./Pages/DetailMovie/DetailMovie";
import Layout from "./Layout/Layout";
import Spinner from "./Components/Spinner/Spinner";
import { useSelector } from "react-redux";
import { useEffect } from "react";
import { useState } from "react";
import TicketRoom from "./Pages/TicketRoom/TicketRoom.jsx";
function App() {
  let { isLoading } = useSelector((state) => state.spinnerReducer);
  return (
    <div>
      {isLoading && <Spinner />}
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/detail/:id" element={<DetailMovie />} />
          <Route path="/chitietphongve/:maLichChieu" element={<TicketRoom />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
