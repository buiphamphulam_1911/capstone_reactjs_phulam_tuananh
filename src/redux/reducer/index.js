import { combineReducers } from "redux";
import { spinnerReducer } from "./reducerSpinner";
import { userReducer } from "./reducerUser";
import { ticketReducer } from "./reducerTicket";
export let rootRedcuer = combineReducers({
  userReducer,
  spinnerReducer,
  ticketReducer,
});
