import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { movieServ } from "../../Services/movieServices";
import ItemTabsMovie from "../HomePage/ItemTabsMovie";
import moment from "moment";
import { NavLink } from "react-router-dom";
import { localServ } from "../../Services/localService";

export default function TabsDetailMovie({ id }) {
  const [dataMovies, setDataMovies] = useState([]);
  useEffect(() => {
    movieServ
      .layThongTinLichChieuPhim(id)
      .then((res) => {
        setDataMovies(res.data.content.heThongRapChieu);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderContent = () => {
    return dataMovies.map((heThongRap, index) => {
      return (
        <Tabs.TabPane
          tab={<img className="w-16 h-16  " src={heThongRap.logo} />}
          key={index}
        >
          <Tabs style={{ height: 500 }} tabPosition="left">
            {heThongRap.cumRapChieu.map((cumRap, index) => {
              return (
                <Tabs.TabPane
                  tab={
                    <div className="w-48 text-left ">
                      <p className="text-white truncate hover:text-slate-500 ">
                        {cumRap.tenCumRap}
                      </p>
                      <p className="truncate text-white hover:text-slate-500  ">
                        {cumRap.diaChi}
                      </p>
                    </div>
                  }
                  key={index}
                >
                  <div
                    style={{ height: 500, overflowY: "scroll" }}
                    className="h-32 scrollbar-thin scrollbar-thumb-green-700
                     scrollbar-track-green-300 overflow-y-scroll 
                     scrollbar-thumb-rounded-full scrollbar-track-rounded-full"
                  >
                    <div className="grid grid-cols-4 gap-3">
                      {cumRap.lichChieuPhim.map((phim) => {
                        let maLichChieu = phim.maLichChieu;
                        return (
                          <NavLink
                            className="btn buyTicket__button"
                            to={`/chitietphongve/${maLichChieu}`}
                          >
                            <div className=" p-3 rounded bg-green-600 text-white text-center hover:bg-red-500 ">
                              {moment(phim.ngayChieuGioChieu).format(
                                "DD-MM-YYYY ~ hh:mm"
                              )}
                            </div>
                          </NavLink>
                        );
                      })}
                    </div>
                  </div>
                </Tabs.TabPane>
              );
            })}
          </Tabs>
        </Tabs.TabPane>
      );
    });
  };
  return (
    <div>
      <Tabs style={{ height: 500 }} tabPosition="left" defaultActiveKey="1">
        {renderContent()}
      </Tabs>
    </div>
  );
}
