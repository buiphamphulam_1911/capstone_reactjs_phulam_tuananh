import React from 'react'
import { useSelector } from 'react-redux';
import {PacmanLoader} from "react-spinners"
import "./Spinner.css"
export default function Spinner() {
  let {isLoading} = useSelector((state)=>state.spinnerReducer);
  return isLoading ? (
    // style={{marginTop:0}} className="h-screen w-screen flex justify-center items-center fixed left-0 top-0 right-0 bg-black z-50"
    <div className="loader " >
      <PacmanLoader color="#ffb702" />
    </div>
  ):(
    <></>
  )
}
